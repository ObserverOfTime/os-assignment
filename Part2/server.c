#include <arpa/inet.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "common.h"

/** Maximum number of simultaneous clients */
#define MAX_CLIENTS 5

/** Error message for failed commands */
#define ERR_MSG "Failed to execute given command.\n"

/** Data structure representing a client */
typedef struct {
    char addr[INET_ADDRSTRLEN]; /**< The client's IP address */
    int sockfd; /**< The client's socket file descriptor */
    int cmds; /**< The client's successfully executed commands */
} Client;

/** The server's socket file descriptor */
static int server_sockfd;

/** The server's PID */
static int server_pid;

/**
 * Signal handler
 * @param signum the signal that was received
 */
void sighandler(int signum);

/**
 * Handle a connected client
 * @param client the client
 */
void handle_client(Client client);

/**
 * Execute a command and return the exit code
 * @param cmd the command to execute
 * @return the exit code of the command
 */
int exec_cmd(const char *cmd);

int main(int argc, char **argv) {
    USAGE(1, "[PORT]");

    struct sockaddr_in serv_addr, cli_addr;
    socklen_t cli_len = sizeof cli_addr;

    // Create server
    server_sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(server_sockfd < 0) DIE("opening socket");
    memset(&serv_addr, 0, sizeof serv_addr);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons((uint16_t) strtol(argv[1], NULL, 10));
    if(bind(server_sockfd, (struct sockaddr *) &serv_addr,
                sizeof serv_addr) < 0) DIE("on binding");
    server_pid = getpid();

    // Listen for connections
    if(listen(server_sockfd, MAX_CLIENTS) < 0) DIE("on listening");
    LOG("Listening for connections on port %s...", argv[1]);

    // Exit on SIGINT
    signal(SIGINT, sighandler);

    for(;;) {
        // Accept new client
        Client client;
        client.sockfd = accept(server_sockfd,
                (struct sockaddr *) &cli_addr, &cli_len);
        if(client.sockfd < 0) DIE("on accepting");
        if(inet_ntop(AF_INET, &cli_addr.sin_addr,
                    client.addr, INET_ADDRSTRLEN) == NULL) {
            LOG("ERROR: could not convert byte to address.");
            exit(EXIT_FAILURE);
        }
        client.cmds = 0;
        handle_client(client);
    }
}

void handle_client(Client client) {
    ssize_t buf_size;
    char buffer[MAX_LENGTH];
    int status, pid = fork();
    switch(pid) {
        case -1: // Fork failed
            DIE("on fork");
        case 0: // Handle client
            LOG("Connection accepted.");
            LOG("The client's address is: %s", client.addr);
            for(;;) {
                buf_size = recv(client.sockfd, buffer, MAX_LENGTH, 0);
                if(buf_size < 0) DIE("reading from socket");
                if(buf_size == 0) {
                    LOG("Client disconnected");
                    break;
                }
                // Strip newline from buffer
                buffer[strcspn(buffer, "\n")] = 0;

                // Handle client exit
                if(STR_EQ(buffer, "exit")) {
                    LOG("Client exited");
                    close(client.sockfd);
                    break;
                }

                // Play game and end connection
                if(STR_EQ(buffer, "END")) {
                    srand((unsigned) getpid());
                    uint16_t numbers[2] = {
                        htons((uint16_t) client.cmds),
                        htons((uint16_t) (rand() % (MAX_NUMBER + 1)))
                    };
                    send(client.sockfd, &numbers, sizeof numbers, 0);
                    close(client.sockfd);
                    break;
                }

                LOG("Executing: %s", buffer);
                // Execute command and send output to client
                dup2(client.sockfd, STDOUT_FILENO);
                int exit_code = exec_cmd(buffer);
                if(exit_code == 0) ++(client.cmds);
                else send(client.sockfd, ERR_MSG, strlen(ERR_MSG), 0);
                LOG("Exit code: %d", exit_code);
                send(client.sockfd, "\0", 1, 0);
                if(buf_size < 0) DIE("writing to socket");
            }
            break;
        default: // Close client
            waitpid(pid, &status, WNOHANG);
            close(client.sockfd);
    }
}

int exec_cmd(const char *cmd) {
    int status, pid = fork();
    switch(pid) {
        case -1: // Exit if fork failed
            DIE("on executing command");
        case 0: // Execute command
            execl("/bin/sh", "sh", "-c", cmd, NULL);
        default: // Return status
            waitpid(pid, &status, WUNTRACED);
            return status >> 8;
    }
}

void sighandler(int signum) {
    close(server_sockfd);
    // Exit children first
    if(getpid() != server_pid)
        _exit(signum);
    LOG("\nExiting server...");
    exit(EXIT_SUCCESS);
}

