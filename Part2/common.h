#ifndef _COMMON_H
#define _COMMON_H

/** Maximum command length */
#define MAX_LENGTH 1024

/** Maximum number in game */
#define MAX_NUMBER 20

/**
 * Check if two strings are equal
 * @param s1 the first string
 * @param s2 the second string
 */
#define STR_EQ(s1, s2) (strcmp(s1, s2) == 0)

/**
 * Log a message to stderr
 * @param msg the message to log
 * @param ... extra log parameters
 */
#define LOG(msg, ...) fprintf(stderr, msg"\n", ##__VA_ARGS__)

/**
 * Print the last error and exit
 * @param err an error message
 */
#define DIE(err) \
    do { \
        perror("ERROR "err); \
        exit(EXIT_FAILURE); \
    } while(0)

/**
 * Print a usage message for the program
 * @param num the number of required arguments
 * @param desc a description of the arguments
 */
#define USAGE(num, desc) \
    do { \
        if(argc < (num) + 1) { \
            LOG("Usage: %s "desc, argv[0]); \
            exit(2); \
        } else if(argc > (num) + 1) { \
            LOG("WARNING: Excessive arguments ignored."); \
        } \
    } while(0)

#endif /* ifndef _COMMON_H */

// vim:ft=c:

