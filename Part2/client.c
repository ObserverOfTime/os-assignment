#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "common.h"

/**
 * Print a shell prompt
 * @param addr the server address
 */
#define PROMPT(addr) printf("%s_> ", addr)

/** The socket file descriptor */
static int sockfd;

/**
 * Signal handler
 * @param signum the signal that was received
 */
void sighandler(int signum);

int main(int argc, char **argv) {
    USAGE(2, "[IP ADDRESS] [PORT]");

    // Normalise localhost address
    if(STR_EQ(argv[1], "localhost") || STR_EQ(argv[1], "0.0.0.0"))
        argv[1] = "127.0.0.1";

    size_t buf_size = 0;
    ssize_t sent = 0, rec = 0;
    struct sockaddr_in serv_addr;
    char *input = NULL, output[2];

    // Create client
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd < 0) DIE("opening socket");
    memset(&serv_addr, 0, sizeof serv_addr);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(argv[1]);
    serv_addr.sin_port = htons((uint16_t) strtol(argv[2], NULL, 10));

    // Connect to server
    if(connect(sockfd, (struct sockaddr *) &serv_addr,
                sizeof serv_addr) < 0) DIE("connecting");
    LOG("Connected to server %s", argv[1]);

    // Exit on SIGINT
    signal(SIGINT, sighandler);

    for(;;) {
        PROMPT(argv[1]);

        // Read command
        getline(&input, &buf_size, stdin);
        if(buf_size > MAX_LENGTH) {
            LOG("ERROR: input cannot exceed %d characters", MAX_LENGTH);
            free(input);
            continue;
        }

        // Deal with EOF (Ctrl-D)
        if(feof(stdin)) {
            clearerr(stdin);
            free(input);
            LOG("^D");
            continue;
        }

        // Send command to server
        sent = send(sockfd, input, buf_size, 0);
        if(sent < 0) DIE("on sending");

        // Handle exit command
        if(STR_EQ(input, "exit\n")) {
            free(input);
            close(sockfd);
            LOG("Exiting");
            exit(EXIT_SUCCESS);
        }

        // Play game and end connection
        if(STR_EQ(input, "END\n")) {
            free(input);
            char *result = "lost";
            uint16_t picked, numbers[2];

            rec = recv(sockfd, &numbers, sizeof numbers, MSG_WAITALL);
            if(rec != sizeof numbers) DIE("receiving number");
            numbers[0] = ntohs(numbers[0]);
            numbers[1] = ntohs(numbers[1]);

            srand((unsigned) getpid());
            printf("Your numbers: ");
            for(int i = 0; i < numbers[0]; ++i) {
                picked = (uint16_t) (rand() % (MAX_NUMBER + 1));
                if(picked > numbers[1]) result = "won";
                if(i == 0) printf("%d", picked);
                else printf(", %d", picked);
            }
            printf("\nThe server's number: %d\n", numbers[1]);
            printf("You %s the game!\n", result);

            close(sockfd);
            exit(EXIT_SUCCESS);
        }

        memset(input, 0, buf_size);

        do { // Read byte per byte until terminated
            memset(output, 0, 2);
            rec = read(sockfd, output, 1);
            if(rec < 0) DIE("reading from socket");
            if(rec == 0) {
                LOG("ERROR: disconnected from server");
                exit(EXIT_FAILURE);
            }
            printf("%s", output);
        } while(output[0] != '\0');
    }
    free(input);
}

void sighandler(int signum) {
    close(sockfd);
    LOG("\nDisconnecting from server...");
    exit(signum);
}

