#!/bin/bash

dir="${1?:Please provide a directory}"
declare -a results # Array holding the results

# Repeat a character
repeat() { printf "%$2s" | tr ' ' "$1"; }

# Print file info delimited by tabs
file_info() {
  printf '%s\t%s\t%s' "$1" \
    "$(file -b "$1")" \
    "$(du -B1 -sh "$1" | cut -f1)"
}

# Add the info of each file to the results
while read -rd '' file; do
  results+=("$(file_info "$file")")
done < <(GLOBIGNORE=.; LC_ALL=C;
  find "${dir%%/}"/* -print0 2>/dev/null | sort -z)

if [ ${#results} -eq 0 ]; then
  printf 'No files found.\n' >&2; exit 1
fi

# Sort and format results
readarray -t formatted < <(IFS=$'\n'; \
  printf 'Name\tType\tSize\n%s\n' \
  "${results[*]}" | column -t -s $'\t')

# Print formatted result table
printf '%s\n' "${formatted[0]}" # Header
repeat '-' "${#formatted[0]}"; printf '\n'
printf '%s\n' "${formatted[@]:1}" # Rows
repeat '=' "${#formatted[0]}"; printf '\n'

# Print total size
total="$(du -B1 -sh "$dir" | cut -f1)"
printf 'Total size of directory: %s\n' "$total"

