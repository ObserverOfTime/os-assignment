#!/bin/bash

team="${1:?Please provide a team}"
user="${2:?Please provide a username}"
dir="${3:?Please provide a directory}"
declare -a results # Array holding the results

# Check whether username is present in file
check_presence() {
  if grep -q "^$1" "$2"; then
    ((++presence)); return 0
  fi
  ((++absence)); return 1
}

# Check each team file in the directory
for file in "$dir"/*"$team"; do
  filename="${file##*/}"
  check_presence "$user" "$file"
  results+=("${filename%%_$team}: $?")
done

# Print the results
printf '%s: presence: %d absence: %d\n' \
  "$user" "$presence" "$absence"
printf '%s\n' "${results[@]}"

