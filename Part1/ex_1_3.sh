#!/bin/bash

QUOTA=$((5*1024**3)) # 5GB
size="$(du -B1 -s ~ | cut -f1)"
used="$(bc -l <<< "$size/$QUOTA*100")"

# Print percentage with precision of 2 decimal digits
printf 'You are using %.2f%% of your storage!\n' "$used"

