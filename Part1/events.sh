#!/bin/bash

FILE="$(readlink -f "${0%/*}")/events.csv"

MENU="\
1. Insert Event
2. Delete Event
3. Modify Event
4. Search Events
5. Preview Events
6. Preview Day Events
7. Quit
"
# Associative array of prompt messages
declare -A PROMPTS=( \
  [insert]='Would you like to insert another event?' \
  [delete]='Would you like to delete another event?' \
  [modify]='Would you like to modify another event?' \
  [search]='Would you like to search for another event?' \
  [preview]='Would you like to preview all events again?' \
  [preview_day]="Would you like to preview another day's event?" \
  [quit]='Are you sure you want to quit?' \
  [other]='Would you like to do anything else?' \
)

# Print an error to stderr
error() { printf '%s\n' "$1" >&2; }

# Prompt user to retry after error
retry() { error "$1 Please try again."; }

# Parse event using awk with special pattern
# to treat escaped commas as part of the field
parse() { awk -vFPAT='(\\\\.|[^,])*' "{print \$$1}" <<< "$2"; }

# Replace event in file
replace() { sed -i "$FILE" -e ":a;\$!{N;ba};s/$1/$2/$3"; }

# Escape string
escape() {
  : "${1//\\/\\\\\\\\}"
  : "${_//\//\\\\\/}"
  printf '%s' "${_//${2:-&}/\\\\${2:-&}}"
}

# Prompt user with a yes/no question
yesno() {
  declare answer
  while :; do
    read -rp "$1 (yes/no) " answer
    case "$answer" in
      [Yy][Ee][Ss]|[Yy]) return 0 ;;
      [Nn][Oo]|[Nn]) return 1 ;;
      *) error 'Please answer yes or no.' ;;
    esac
  done
}

# Get date in given format
get_date() {
  declare date
  read -rp 'Input the date of the event: ' date
  until [[ $date =~ ^[0-9]{${#1}}$ ]]; do
    retry "The date must be in $1 format."
    read -rp 'Input the date of the event: ' date
  done
  printf '%s' "$date"
}

# Insert event to file
insert_event() {
  declare name desc date="$(get_date ddmmyyyyHHMM)"
  read -rp 'Input the name of the event: ' name
  read -rp 'Input a description for the event: ' desc
  printf '%s,%s,%s\n' >> "$FILE" \
    "$date" "${name//,/\\,}" "${desc//,/\\,}"
}

# Delete event from file
delete_event() {
  declare e date="$(get_date ddmmyyyyHHMM)"
  declare -a events
  readarray -t events < <(grep "^$date" "$FILE")
  if [ ${#events} -eq 0 ]; then
    error "No events were found for ${date}."; return 1
  fi
  for e in "${!events[@]}"; do
    printf 'Found: %s\n' "${events[$e]}"
    if yesno 'Would you like to delete this event?'; then
      awk -vevt="$(escape "${events[$e]}" '$')" -vn="$e" \
        '$0 !~ evt || i++ != n' "$FILE" | tee "$FILE" >/dev/null
    fi
  done
}

# Modify event in file
modify_event() {
  declare date="$(get_date ddmmyyyyHHMM)"
  declare -a events
  readarray -t events < <(grep "^$date" "$FILE")
  if [ ${#events} -eq 0 ]; then
    error "No events were found for ${date}."; return 1
  fi
  # Keep a counter of identical events
  declare e f reply previous counter=1
  declare -a old new fields=(date name description)
  for e in "${!events[@]}"; do
    old=( \
      "$(parse 1 "${events[$e]}")" \
      "$(parse 2 "${events[$e]}")" \
      "$(parse 3 "${events[$e]}")" \
    )
    printf 'Found: %s\n' "${events[$e]}"
    [ "${previous=}" = "${events[$e]}" ] && ((++counter)) || counter=1
    if yesno 'Would you like to modify this event?'; then
      for f in "${!fields[@]}"; do
        printf 'Input a new %s for the event.\n' "${fields[$f]}"
        read -rp '(Leave blank to keep the current): ' reply
        # If the reply is blank keep the old value
        [ -z "$reply" ] && new[$f]="${old[$f]}" && continue
        case "${fields[$f]}" in
          date)
            until [[ $reply =~ ^[0-9]{12}$ ]]; do
              retry 'The date must be in ddmmyyyyHHMM format.'
              read -rp 'Input a new date for the event: ' reply
              [ -z "$reply" ] && reply="${old[$f]}"
            done
            new[$f]="$reply"
            ;;
          name) new[$f]="${reply//,/\\,}" ;;
          description) new[$f]="${reply//,/\\,}" ;;
        esac
      done
      replace "$(escape "${events[$e]}")" "$(escape \
        "${new[0]},${new[1]},${new[2]}")" "$counter"
    fi
    previous="${events[$e]}"
  done
}

# Find event in file
search_event() {
  declare query
  read -rp 'Input the date or name to search for: ' query
  grep -F "$query" "$FILE" || error "No events found for $query."
}

# Preview all future events
preview_events() {
  declare today; printf -v today '%(%d%m%Y)T'
  awk -F, -vd="${today:0:2}" -vm="${today:2:2}" -vy="${today:4:4}" '{
    day=substr($1,1,2); month=substr($1,3,2); year=substr($1,5,4)
    if (year < y || (year == y && month < m)) next
    if (year == y && month == m && day < d) next
    rc = 1; print } END { exit !rc }' "$FILE" | sort
  # Print error if awk failed
  ((PIPESTATUS[0])) && error 'No events found.'
}

# Preview events for a day
preview_day_events() {
  declare day="$(get_date ddmmyyyy)"
  grep "^$day" "$FILE" | sort
  # Print error if grep failed
  ((PIPESTATUS[0])) && error "No events found for $day."
}

# Execute user choice
exec_choice() {
  $1 # Run callback function
  while yesno "${PROMPTS[${1%_*}]}"; do $1; done
  yesno "${PROMPTS[other]}" || exit 0
}

# Main program
if ! touch "$FILE" 2>/dev/null; then
  error "Failed to access $FILE!"; exit 1
fi
printf '%s' "$MENU"
while :; do
  read -rp 'Pick one of the above numbers: ' choice
  case "$choice" in
    1) exec_choice insert_event ;;
    2) exec_choice delete_event ;;
    3) exec_choice modify_event ;;
    4) exec_choice search_event ;;
    5) exec_choice preview_events ;;
    6) exec_choice preview_day_events ;;
    7) yesno "${PROMPTS[quit]}" && exit 0 ;;
    *) retry 'Invalid choice.' ;;
  esac
done

